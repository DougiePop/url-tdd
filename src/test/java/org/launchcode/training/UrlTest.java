package org.launchcode.training;

import org.junit.Test;

import static junit.framework.TestCase.fail;
import static org.junit.Assert.assertEquals;

public class UrlTest {

    @Test
    public void isUrlProtocolCorrect(){
        Url pr = new Url("https://launchcode.org/learn");
        String proto = pr.getProtocol();
        assertEquals("https", proto);
    }

    @Test(expected = IllegalArgumentException.class)
    public void isUrlProtocolEmpty(){
        Url pr = new Url("//launchcode.org/learn");
        String proto = pr.getProtocol();
        fail("shouldn't get here");
    }

    @Test
    public void isCapitalUrlProtocolCorrect(){
        Url pr = new Url("HTTPS://LAUNCHCODE.ORG/LEARN");
        String proto = pr.getProtocol();
        assertEquals("https", proto);
    }

    @Test
    public void isUrlDomainCorrect(){
        Url dom = new Url("https://launchcode.org/learn");
        String domain = dom.getDomain();
        assertEquals("launchcode.org", domain);
    }

    @Test
    public void isCapitalUrlDomainCorrect(){
        Url dom = new Url("HTTPS://LAUNCHCODE.ORG/LEARN");
        String domain = dom.getDomain();
        assertEquals("launchcode.org", domain);
    }

    @Test
    public void isUrlDomainFormatCorrect(){
        Url dom = new Url("HTTPS://LAUNCHCODE-.ORG/LEARN");
        String domain = dom.getDomain();
        assertEquals("launchcode-.org", domain);
    }

    @Test
    public void isUrlDomainFormatCorrectPartTwo(){
        Url dom = new Url("HTTPS://LAUNCHCODE_.ORG/LEARN");
        String domain = dom.getDomain();
        assertEquals("launchcode_.org", domain);
    }

    @Test(expected = IllegalArgumentException.class)
    public void isUrlDomainFormatCorrectPartThree(){
        Url dom = new Url("HTTPS://LAUNCHCODE*.ORG/LEARN");
        String domain = dom.getDomain();
        fail("shouldn't get here");
    }

    @Test(expected = IllegalArgumentException.class)
    public void isUrlDomainEmpty(){
        Url dom = new Url("HTTPS:///LEARN");
        String domain = dom.getDomain();
        fail("shouldn't get here");
    }

    @Test
    public void isUrlPathCorrect(){
        Url pa = new Url("https://launchcode.org/learn");
        String path = pa.getPath();
        assertEquals("/learn", path);
    }

    @Test
    public void isCapitalUrlPathCorrect(){
        Url pa = new Url("HTTPS://LAUNCHCODE.ORG/LEARN");
        String path = pa.getPath();
        assertEquals("/learn", path);
    }

    @Test
    public void isUrlMultiplePathCorrect(){
        Url pa = new Url("https://launchcode.org/learn/cool");
        String path = pa.getPath();
        assertEquals("/learn/cool", path);
    }

    @Test
    public void isUrlNoPathCorrect(){
        Url pa = new Url("https://launchcode.org/");
        String path = pa.getPath();
        assertEquals("", path);
    }

    //Blake comments
    @Test
    public void testConstructorConvertsCaseToLower() {
        String urlStr = "HTTPS://lAuNcHcOdE.org/LEARN";
        Url lcUrl = new Url(urlStr);
        String url = lcUrl.getUrl();
        assertEquals("https://launchcode.org/learn", url);
    }

    @Test
    public void testToStringIdealFormat() {
        String urlStr = "https://launchcode.org/learn";
        Url lcUrl = new Url(urlStr);
        String url = lcUrl.getUrl();
        assertEquals(urlStr, url);
    }

}
