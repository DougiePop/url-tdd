package org.launchcode.training;

// "HTTPS://LAUNCHCODE.ORG/LEARN"
// domain = "LAUNCHCODE.ORG"
// protocol = "HTTPS:"
// path = "/LEARN"

public class Url {

    private final String domain;
    private final String protocol;
    private final String path;
    private String url;

    public Url(String url){
        this.url = url.toLowerCase();
        String[] strs = this.url.split("/");
        StringBuilder buf = new StringBuilder();

        for (int i=3; i<strs.length; i++){
            buf.append('/' + strs[i]);
        }
        protocol = strs[0].replace(":", "");
        domain = strs[2];
        path = buf.toString();

        if ((!protocol.equals("ftp")) && (!protocol.equals("http")) && (!protocol.equals("https")) && (!protocol.equals("file"))){
            throw new IllegalArgumentException();
        }

        if (domain.length() == 0 || !domain.matches("[\\$\\_\\-\\._a-zA-Z-0-9]*")){
            throw new IllegalArgumentException();
        }

    }


    public String getDomain() {
        return domain;
    }

    public String getProtocol() {
        return protocol;
    }

    public String getPath() {
        return path;
    }

    public String getUrl() { return url; }
}
